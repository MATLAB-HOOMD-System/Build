function system=BuildPhaseSeparated(numA,numB,numC,numD,ABfrac,xmlname)
system=[];

% numA=4;
% numB=4;
% numC=4;
% numD=4;
% ABfrac=0.1;

nchainsAB=round(((30^3)*3/8/1)*ABfrac);
nchainsCD=round(((30^3)*3/8/1)*(1-ABfrac));
dim=[60 60 60]/2;
xdiv=0;
cutoff=-dim(1)/2+ABfrac*30;



[pos bonds angles attype]=Fill_Box_BCP(numC,numD,nchainsCD,dim);
mass=ones(size(attype));
% clear angles;
angles=[];
attype(attype=='A')='C';
attype(attype=='B')='D';

systemcompress_script


stats=Chain_Statistics(0,system);

for i=1:nchainsCD
    beads=stats.chains2(:,i);
    if sum(pos(beads,1)>cutoff)==0
        pos(beads,1)=pos(beads,1)+cutoff+dim(1)/2;
    end
end
savename='CDbuild';
systemcompress_script
systemexpand_script
save_simulation


[pos bonds angles attype]=Fill_Box_BCP(numA,numB,nchainsAB,dim);
mass=ones(size(attype));
% clear angles;
angles=[];


systemcompress_script


stats=Chain_Statistics(0,system);

for i=1:nchainsAB
    beads=stats.chains2(:,i);
    if sum(pos(beads,1)<cutoff)==0
        pos(beads,1)=(pos(beads,1)-cutoff)*((ABfrac)/(1-ABfrac))-dim(1)/2;
    end
end
savename='ABbuild';
systemcompress_script
systemexpand_script
save_simulation

system=Combine_Fills('ABbuild','CDbuild');
systemexpand_script
savename='build';
save_simulation
Write_XML(savename,xmlname)