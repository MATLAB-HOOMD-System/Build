clear pos bonds angles attype
Nchains=100;
Nbeadsperchain=8;
Abeadsperchain=400;
Bbeadsperchain=4;
bondlength=1;
dim=[10 10 10];
nbeads=1;
mindist=0.1;
bonds=[];

postemp=rand(1,3)*10;
pos(nbeads,:)=postemp;
attype(nbeads)='A';
for i=2:Abeadsperchain
    nbeads=nbeads+1;
    test=1;
    while test==1
        r=bondlength;
        phi=pi*rand();
        theta=2*pi*rand();
        xtemp=r*sin(theta)*cos(phi);
        ytemp=r*sin(theta)*sin(phi);
        ztemp=r*cos(theta);
        x=xtemp+pos(end,1);
        y=ytemp+pos(end,2);
        z=ztemp+pos(end,3);
        if x >dim(1)
            x=x-dim(1);
        end
        if x<0
            x=x+dim(1);
        end
        if y >dim(2)
            y=y-dim(2);
        end
        if y<0
            y=y+dim(2);
        end
        if z >dim(3)
            z=z-dim(3);
        end
        if z<0
            z=z+dim(3);
        end
        dels=pos-repmat([x y z],size(pos,1),1);
        dels2=
        distances=sqrt(sum(dels.^2,2));
        if min(distances)<0.1
            test=1;
        else
            test=0;
            pos=[pos; x y z];
            bonds=[bonds; (nbeads-1), nbeads];
            attype(end+1)='A';
        end
    end
end
        