function [system]=Fill_Box_BCP_new(numA,numB,nchains,dim,bondlength,angle,mass)

rng('shuffle');
if ~exist('bondlength','var')
    bondlengthA=1;
    bondlengthB=1;
else
    bondlengthA=bondlength(1);
    bondlengthB=bondlength(2);
end

if ~exist('angle','var')
    angleA=120*pi/180;
    angleB=120*pi/180;
else
    angleA=angle(1);
    angleB=angle(2);
end

if ~exist('mass','var')
    massA=1;
    massB=1;
else
    massA=mass(1);
    massB=mass(2);
end

nat=(numA+numB)*nchains;
pos=zeros(nat,3);
bonds=zeros(nat-nchains,2);
angles=zeros(nat-2*nchains,3);
attype=char(zeros(nat,1));



m=0; %Atom Number in Chain
n2=0; %Bond Reference
k=0; %Angle Reference
b=[0; 0; 0];
for i=1:nat
    m=m+1;
    if m==1
        pos(i,:)=rand(1,3).*dim;
    elseif m>=2
        n2=n2+1;
        bonds(n2,:)=[i-2 i-1];
        if m==2
            theta=2*pi*rand(1);
            phi=pi*rand(1);
            pos(i,1)=pos(i-1,1)+bondlengthA*cos(theta)*sin(phi);
            pos(i,2)=pos(i-1,2)+bondlengthA*sin(theta)*sin(phi);
            pos(i,3)=pos(i-1,3)+bondlengthA*cos(phi);
        elseif m>2
            theta=2*pi*rand(1);
            if m<=numA
                phi=pi-angleA;
                bondlength=bondlengthA;
            else
                phi=pi-angleB;
                bondlength=bondlengthB;
            end
            b(1,1)=bondlength*cos(theta)*sin(phi);
            b(2,1)=bondlength*sin(theta)*sin(phi);
            b(3,1)=bondlength*cos(phi);
            v2=pos(i-2,:)-pos(i-1,:);
            n=cross([0 0 -1],v2);
            n=n./sqrt(sum(n.^2));
            c=dot([0 0 -1],v2)/sqrt(sum(v2.^2));
            s=sqrt(1-c^2);
            R=[c+n(1)*n(1)*(1-c)       n(1)*n(2)*(1-c)-n(3)*s n(1)*n(3)*(1-c)+n(2)*s;
                n(2)*n(1)*(1-c)+n(3)*s c+n(2)*n(2)*(1-c)      n(2)*n(3)*(1-c)-n(1)*s;
                n(3)*n(1)*(1-c)-n(2)*s n(3)*n(2)*(1-c)+n(1)*s c+n(3)*n(3)*(1-c)];
            pos(i,:)=(R*b)'+pos(i-1,:);
        end
    end

    
    if m>=3
        k=k+1;
        angles(k,:)=[i-3,i-2,i-1];
    end
    
    if m<=numA
        attype(i,1)='A';
        mass(i)=massA;
    else
        attype(i,1)='B';
        mass(i)=massB;
    end
    
    if m==numA+numB
        m=0;
    end
    
end

pos=packing(pos,bonds,dim,[1,1,1]);

system.dim=dim;
system.attype=attype;
system.mass=mass;
system.bonds=bonds;
system.angles=angles;
system.pos=pos;

end