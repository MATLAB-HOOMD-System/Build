% An example script to build simulations. In this case I am building a box
% that is periodic in the x and y dimensions, but not in z. There is an
% underlayer placed in the box with a film placed on top of it. The
% underlayer is a pattern brush underlayer.

% At the end of the file, an example of how to extract xml and dcd data and
% perform chain stats is shown

clear all
close all
clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Build a periodic Box

% Define the shape of the box, in units of nm.
dim=[12.32*6 12.32*3 20]; % [xdim ydim zdim]

% Define Parameters for sim
numA=8;      % Number of A beads per chain
numB=8;      % Number of B beads per chain
N=numA+numB; % Number of beads per chain. In our coarse grained model, this is 1/4 the degree of polymerization (N)
density=1.4; % Density beads in the simulation box (beads/nm^3)
nchains=round(prod(dim)*density/N); % Number of chains needed to make the box have the required density. Needs to be an integer.
[pos bonds angles attype]=Fill_Box_BCP(numA,numB,nchains,dim);

% Save variables under a temporary filename
savename='temp';
save_simulation; % Saves all relevant simulation data under the name savename.mat
% clear pos attype bonds angles numA numB N density nchains dim % Clears all variables but dim to make sure we do not make a mistake when we make the film

% Write XML
Write_XML(savename,['teststart2.xml'])

% Open XML (works for windows computers where vmd is installed as the
% default viewer for xml files)
% winopen([savename '.xml'])

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Build a film


% Define the shape of the box, in units of nm.
% The x and y dimensions will be used when making an underlayer, and
% all three will be used when making a film.
dim=[60 30 10]; % [xdim ydim zdim]

% Specify parameters for the patterned brush underlayer
Adim=5;     % Width of First Stripe
Afrac=1;    % Percent A beads in First Stripe
Bdim=15;    % Width of Second Stripe
Bfrac=0.3;  % Percent B beads in Seconds Stripe

% Generate patterned brush underlayer
[pos attype bonds angles nactive]=PatternBrush2(dim,Adim,Afrac,Bdim,Bfrac);

% Save variables under a temporary filename
savename='temp_underlayer';
save_simulation; % Saves all relevant simulation data under the name savename.mat
clear pos attype bonds angles nactive Adim Afrac Bdim Bfrac % Clears all variables but dim to make sure we do not make a mistake when we make the film


% Define Parameters for Film
numA=7;      % Number of A beads per chain
numB=9;      % Number of B beads per chain
N=numA+numB; % Number of beads per chain. In our coarse grained model, this is 1/4 the degree of polymerization (N)
density=1.4; % Density beads in the simulation box (beads/nm^3)
nchains=round(prod(dim)*density/N); % Number of chains needed to make the box have the required density. Needs to be an integer.
[pos bonds angles attype]=Fill_Box_BCP(numA,numB,nchains,dim);

% Save variables under a temporary filename
savename='temp_film';
save_simulation; % Saves all relevant simulation data under the name savename.mat
clear pos attype bonds angles numA numB N density nchains dim % Clears all variables but dim to make sure we do not make a mistake when we make the film


% Combind film and underlayer data into one simulation
savename='2x_Density_Multiplication_Example';
Splice_Underlayer_and_Film('temp_film','temp_underlayer',savename,-0.25);

% Extract nactive
nactive=load(savename,'nactive');

% Write XML
Write_XML(savename,[savename '.xml'])

% Open XML (works for windows computers where vmd is installed as the
% default viewer for xml files)
winopen([savename '.xml'])



% Extract XML data and run chain stats
clear all
filename='Cylinder_Test_numA=5_numB=11';
Extract_XML([filename '_final.xml'],'temp')
load('temp')
chainstats=Chain_Statistics('temp'); % Note, this code performs chain stats 
% on all beads in 'temp'. This means if you have an underlayer it will be 
% included in the calculation unless you remove it beforehand.

% Extract XML and DCD data and run chain stats
clear all
filename='Cylinder_Test_numA=5_numB=11';
Extract_XML_DCD([filename '_final.xml'],[filename '.dcd'],'temp',0)
chainstats=Chain_Statistics('temp');