function [pos attype bonds angles nactive]=PatternBrush2(dim,Adim,Afrac,Bdim,Bfrac)
% Build Brushes with angles to make them more collapsed

bspace=1.5;
nbeads_per_chain=6;
dz=0.83;
theta=120*pi/180;

repx=round(dim(1)/(Adim+Bdim));
Anum=round(Adim/bspace);
Bnum=round((Adim+Bdim)/bspace);
nbeadsx=Bnum*repx;

% Build Fixed Under Beads
% dx=bspace;
% nbeadsx=round(dim(1)/dx);
nbeadsy=round(dim(2)/bspace);
dx=dim(1)/(nbeadsx);
dy=dim(2)/(nbeadsy);

p=0;
fpos=zeros(nbeadsx*nbeadsy,3);
fattype=repmat('A',nbeadsx*nbeadsy,1);
for i=1:nbeadsx
    p=p+1;
    for j=1:nbeadsy
        fpos((i-1)*nbeadsy+j,1)=-dim(1)/2+(i-1)*dx+.001;
        fpos((i-1)*nbeadsy+j,2)=-dim(2)/2+(j-1)*dy+.001;
        if p<=Anum
            if rand(1)>Afrac
                fattype((i-1)*nbeadsy+j,1)='B';
            end
        else
            if rand(1)>Bfrac
                fattype((i-1)*nbeadsy+j,1)='B';
            end
        end
    end
    if p>=Bnum
        p=0;
    end
end


n=0;
m=0;
k=0;
p=0;
pos=zeros(nbeadsx*nbeadsy*6,3);
attype=repmat('A',nbeadsx*nbeadsy*6,1);
bonds=zeros(nbeadsx*nbeadsy*5,2);
angles=zeros(nbeadsx*nbeadsy*4,3);
for x=dx/2-dim(1)/2:dx:dim(1)/2
    p=p+1;
    for y=dy/2-dim(2)/2:dy:dim(2)/2
        for z=1:nbeads_per_chain
            n=n+1;
            if z==1
                pos(n,:)=[x y dz];
            elseif z==2
                phi=2*pi*rand(1);
                pos(n,:)=pos(n-1,:)+[cos(phi)*cos(theta-pi/2) sin(phi)*cos(theta-pi/2) dz*sin(theta-pi/2)];
            else
                q=0;
                while pos(n,3)<dz
                    q=q+1;
                    if q<100
                        theta3=norminv(rand(1),theta/2,30*pi/180);
                    else
                        theta3=norminv(rand(1),theta,30*pi/180);
                    end
                    phi=2*pi*rand(1);
                    change=[cos(phi)*cos(theta3-pi/2) sin(phi)*cos(theta3-pi/2) dz*sin(theta3-pi/2)];
                    origvec=pos(n-1,:)-pos(n-2,:);
                    basevec=[0 0 1];

                    theta2=atan2(origvec(1),origvec(3));
                    basevec=([cos(theta2) 0 sin(theta2); 0 1 0; -sin(theta2) 0 cos(theta2)]*basevec')';
                    change=([cos(theta2) 0 sin(theta2); 0 1 0; -sin(theta2) 0 cos(theta2)]*change')';
                    origvec2=([cos(-theta2) 0 sin(-theta2); 0 1 0; -sin(-theta2) 0 cos(-theta2)]*origvec')';

                    theta2=atan2(origvec2(3),origvec2(2))-atan2(1,0);
                    basevec=([1 0 0; 0 cos(theta2) -sin(theta2); 0 sin(theta2) cos(theta2)]*basevec')';
                    change=([1 0 0; 0 cos(theta2) -sin(theta2); 0 sin(theta2) cos(theta2)]*change')';

                    theta2=atan2(origvec(2),origvec(1))-atan2(basevec(2),basevec(1));
                    change=([cos(theta2) -sin(theta2) 0; sin(theta2) cos(theta2) 0; 0 0 1]*change')';

                    change=change*dz/sqrt(sum(change.^2));
                    pos(n,:)=pos(n-1,:)+change;
                end
            end
            if z>1
                m=m+1;
                bonds(m,1)=n-1;
                bonds(m,2)=n;
            end
            if z>2
                k=k+1;
                angles(k,1)=n-2;
                angles(k,2)=n-1;
                angles(k,3)=n;
            end
            if p<=Anum
                if rand(1)>Afrac
                    attype(n,1)='B';
                end
            else
                if rand(1)>Bfrac
                    attype(n,1)='B';
                end
            end
        end
    end
    if p>=Bnum
        p=0;
    end
end

pos=pos(1:n,:);
pos=[pos; fpos];
attype=[attype; fattype];
bonds=bonds(1:m,:)-1;
angles=angles(1:k,:)-1;

[nat,col]=size(pos);

freeze=zeros(nat,1);
freeze(1:nbeads_per_chain:n,1)=1;
freeze(n+1:nat)=1;

totalfrozen=sum(freeze);
shift=cumsum(freeze);
swap=zeros(nat,1);
pos2=zeros(nat-totalfrozen,3);
attype2=char(zeros(nat-totalfrozen,1));
for i=1:nat
    if freeze(i)==0;
        pos2(i-shift(i),:)=pos(i,:);
        attype2(i-shift(i),1)=attype(i,1);
        swap(i,1)=i-shift(i);
    end
end
pos3=pos(freeze==1,:);
attype3=attype(freeze==1,1);
swap(freeze==1)=nat-totalfrozen+1:nat;
pos=[pos2; pos3];
attype=[attype2; attype3];
bonds(:,1)=swap(bonds(:,1)+1)-1;
bonds(:,2)=swap(bonds(:,2)+1)-1;
angles(:,1)=swap(angles(:,1)+1)-1;
angles(:,2)=swap(angles(:,2)+1)-1;
angles(:,3)=swap(angles(:,3)+1)-1;
nactive=nat-totalfrozen;

end


