function Splice_Underlayer_and_Film(Film_File,Underlayer_File,Save_Name,deltaH)
% Puts together a film an underlayer file at a distance deltaH from the
% highest point on the underlayer to the lowest point on the film.

load(Film_File);
pos=packing(pos,bonds,dim,[1 1 0]);
posfilm=pos;
bondsfilm=bonds;
anglesfilm=angles;
attypefilm=attype;
dimfilm=dim;

clear pos bonds angles attype dim

load(Underlayer_File);
posunder=pos;
bondsunder=bonds;
anglesunder=angles;
attypeunder=attype;
dimunder=dim;

clear pos bonds angles attype dim

zminfilm=min(posfilm(:,3));
zmaxunder=max(posunder(:,3));
posfilm(:,3)=posfilm(:,3)-zminfilm+deltaH/2;
posunder(:,3)=posunder(:,3)-zmaxunder-deltaH/2;

nat1=size(posfilm,1);
bondsunder=bondsunder+nat1;
anglesunder=anglesunder+nat1;
nactive=nactive+nat1;

pos=[posfilm; posunder];
clear posfilm posunder
bonds=[bondsfilm; bondsunder];
clear bondsfilm bondsunder
angles=[anglesfilm; anglesunder];
clear anglesfilm anglesunder
attype=[attypefilm; attypeunder];
clear attypefilm attypeunder
if ~isequal(dimunder(1:2),dimfilm(1:2))
    warning('X and Y dimensions of film are different than underlayer. Will use film dimensions and continue')
end
dim=dimfilm;
clear dimfilm dimunder

zmin=min(pos(:,3));
zmax=max(pos(:,3));
dim(3)=20*(zmax-zmin);

pos=packing(pos,bonds,dim,[1,1,1]);

savename=Save_Name;
save_simulation;

end