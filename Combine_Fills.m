function [system]=Combine_Fills(savename1,savename2)
load(savename1)

angles2=angles;
attype2=attype;
bonds2=bonds;
dim2=dim;
mass2=mass;
pos2=pos;

load(savename2);
if dim2~=dim
    error('Box Sizes are different')
end

angles=[angles angles2+length(attype)];
bonds=[bonds; bonds2+length(attype)];
mass=[mass; mass2];
pos=[pos; pos2];
attype=[attype; attype2];

systemcompress_script