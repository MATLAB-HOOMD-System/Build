function system=strip_beads(FileName,system,striptype,beadnums)
if FileName~=0
    load(FileName)
else
    [attype,bonds,dim,mass,pos,angles]=systemexpand(system);
end


if exist('striptype')==0
    striptype='A';
end


%%%select only A beads
if strcmp(striptype,'A')

    selection = attype=='A';
    selectionnotA = attype~='A';

    bonds2=bonds+1; 
    B=sum((attype(bonds2)=='A')')';
    B=B==2;
    bonds2=bonds2(B,:);
    for i=1:(length(bonds2))
        bonds2(i,:)=bonds2(i,:)-sum(selectionnotA(1:bonds2(i,1)));
    end
    bonds=bonds2-1;


    attype=attype(selection);
    pos=pos(selection,:,:);
    mass=mass(selection);
end

if exist('outputname')
    savename=outputname;
else
    savename='stripped';
end
if FileName~=0
    save_simulation
else
    systemcompress_script
end



%%select A and B beads
if strcmp(striptype,'A-B')

    selection = attype=='A' | attype=='B';
    selectionnotA = attype~='A' & attype~='B';;

    bonds2=bonds+1; 
    B=sum((attype(bonds2)=='A' | attype(bonds2)=='B')')';
    B=B==2;
    bonds2=bonds2(B,:);
    for i=1:(length(bonds2))
        bonds2(i,:)=bonds2(i,:)-sum(selectionnotA(1:bonds2(i,1)));
    end
    bonds=bonds2-1;


    attype=attype(selection);
    pos=pos(selection,:,:);
    mass=mass(selection);
end
%%select B and C beads
if strcmp(striptype,'B-C')

    selection = attype=='C' | attype=='B';
    selectionnotA = attype~='C' & attype~='B';;

    bonds2=bonds+1; 
    B=sum((attype(bonds2)=='C' | attype(bonds2)=='B')')';
    B=B==2;
    bonds2=bonds2(B,:);
    for i=1:(length(bonds2))
        bonds2(i,:)=bonds2(i,:)-sum(selectionnotA(1:bonds2(i,1)));
    end
    bonds=bonds2-1;


    attype=attype(selection);
    pos=pos(selection,:,:);
    mass=mass(selection);
end

%%A C D
if strcmp(striptype,'A-C-D')

    selection = attype=='A' | attype=='C' |attype=='D';
    selectionnotA = attype~='A' & attype~='C' & attype~='D';

    bonds2=bonds+1; 
    B=sum((attype(bonds2)=='A' | attype(bonds2)=='C' | attype(bonds2)=='D')')';
    B=B==2;
    bonds2=bonds2(B,:);
    for i=1:(length(bonds2))
        bonds2(i,:)=bonds2(i,:)-sum(selectionnotA(1:bonds2(i,1)));
    end
    bonds=bonds2-1;


    attype=attype(selection);
    pos=pos(selection,:,:);
    mass=mass(selection);
end

%%
%%select A and B beads
if strcmp(striptype,'A-B')

    selection = attype=='A' | attype=='B';
    selectionnotA = attype~='A' & attype~='B';;

    bonds2=bonds+1; 
    B=sum((attype(bonds2)=='A' | attype(bonds2)=='B')')';
    B=B==2;
    bonds2=bonds2(B,:);
    for i=1:(length(bonds2))
        bonds2(i,:)=bonds2(i,:)-sum(selectionnotA(1:bonds2(i,1)));
    end
    bonds=bonds2-1;


    attype=attype(selection);
    pos=pos(selection,:,:);
    mass=mass(selection);
end


%% Only D beads
if strcmp(striptype,'D')

    selection=attype=='D';
    selectionnotA = attype~='D';

    bonds2=bonds+1; 
    B=sum((attype(bonds2)=='D')')';
    B=B==2;
    bonds2=bonds2(B,:);
    for i=1:(length(bonds2))
        bonds2(i,:)=bonds2(i,:)-sum(selectionnotA(1:bonds2(i,1)));
    end
    bonds=bonds2-1;


    attype=attype(selection);
    pos=pos(selection,:,:);
    mass=mass(selection);
end

%% strip by bead number

if strcmp(striptype,'number')

    selection = ismember(1:length(attype),beadnums);
    selectionnotA = ~selection;

%     bonds=system.bonds;
    bonds2=bonds+1; 
    B1=ismember(bonds(:,1),beadnums);
    B2=ismember(bonds(:,2),beadnums);
    B=sum([B1 B2]')';
    B=B==2;
    bonds2=bonds2(B,:);
    
    
    %got selection, which gets attype, pos and mass, still getting
    %bonds/angles
    for i=1:(length(bonds2))
        bonds2(i,:)=bonds2(i,:)-sum(selectionnotA(1:bonds2(i,1)));
    end
    bonds=bonds2-1;


    attype=attype(selection);
    pos=pos(selection,:,:);
    mass=mass(selection);
end
%%

% if exist('outputname')
%     savename=outputname;
% else
%     savename='stripped';
% end
% if FileName~=0
%     save_simulation
% else
%     systemcompress_script
% end

%%%%%%%only B beads

%%%select only A beads
if strcmp(striptype,'B')

    selection = attype=='B';
    selectionnotA = attype~='B';

    bonds2=bonds+1; 
    B=sum((attype(bonds2)=='B')')';
    B=B==2;
    bonds2=bonds2(B,:);
    for i=1:(length(bonds2))
        bonds2(i,:)=bonds2(i,:)-sum(selectionnotA(1:bonds2(i,1)));
    end
    bonds=bonds2-1;


    attype=attype(selection);
    pos=pos(selection,:,:);
    mass=mass(selection);
end

if exist('outputname')
    savename=outputname;
else
    savename='strippedB';
end
if FileName~=0
    save_simulation
else
    systemcompress_script
end



end